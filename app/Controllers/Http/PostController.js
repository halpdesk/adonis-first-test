'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Post = use('App/Models/Post')

/**
 * Resourceful controller for interacting with posts
 */
class PostController {

  * index ({ request, response, view }) {
    const posts = yield Post.all()
    console.log(request)
    yield response.status(200).json(posts)
  }

  * store ({ request, response }) {
    const data = request.only(['title', 'body'])
    const post = yield Post.create(data)
    yield response.status(201).json(post)
  }

  * show ({ params, request, response }) {
    const data = request.id
    const post = yield Post.findByOrFail('id', data.id)
    yield response.status(201).json(post)
  }

  async update ({ params, request, response }) {
  }

  async destroy ({ params, request, response }) {
  }
}

module.exports = PostController
